import {browserHistory, Router} from 'react-router';
import FastClick from 'fastclick';
import React from 'react';
import {render} from 'react-dom';
import routes from '../../universal/routes';
import './main.sass';

render(
   <Router history={browserHistory}>{routes}</Router>,
   document.getElementById('main')
);

FastClick.attach(document.body);