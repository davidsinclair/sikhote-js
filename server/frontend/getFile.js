export default {
   method: 'get',
   path: '/{p*}',
   handler: {
      directory: {
         path: './browser/dist',
         index: false
      }
   }
};
