import eslint from 'gulp-eslint';
import gulp from 'gulp';
import gutil from 'gulp-util';
import webpack from 'webpack';
import runSequence from 'run-sequence';
import bg from 'gulp-bg';
import shell from 'gulp-shell';
import env from 'gulp-env';
import yargs from 'yargs';

env.set({
   FORCE_COLOR: true,
   NODE_ENV: yargs.argv.d? 'development' : 'production',
   WATCH: yargs.argv.w
});

const {isWatching} = require('./universal/config');
const {default: webpackConfig} = require('./webpack.config.js');
const paths = [
   'browser/src/**/*.js',
   'universal/**/*.js',
   'server/**/*.js',
   'gulpfile.babel.js',
   'webpack.config.js'
];

gutil.log(`${isWatching === true? 'Watching' : 'Starting'} in ` +
    `${process.env.NODE_ENV} environment...`);

gulp.task('browser:build', done => {
   webpack(webpackConfig, (error, stats) => {
      const jsonStats = stats.toJson({chunks:false});
		const buildError = error || jsonStats.errors[0] || jsonStats.warnings[0];

      if(buildError) {throw new gutil.PluginError('webpack', buildError);}

      done();
   });
});

gulp.task('browser:watch', bg('node', 'browser/src/server'));

gulp.task('server:watch', shell.task(
   'nodemon -q --ignore webpack-assets.json server'
));

gulp.task('lint', () => gulp
    .src(paths)
    .pipe(eslint({quiet: true}))
    .pipe(eslint.format())
);

gulp.task('watch', () => {gulp.watch(paths, ['lint']);});

gulp.task('default', done => {
   if(isWatching) {
      runSequence('watch', 'browser:watch', 'server:watch', done);
   } else {
      runSequence('lint', 'browser:build', done);
   }
});