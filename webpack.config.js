import autoprefixer from 'autoprefixer';
import CopyWebpackPlugin from 'copy-webpack-plugin';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import webpack from 'webpack';
import WebpackIsomorphicToolsPlugin from 'webpack-isomorphic-tools/plugin';
import wITConfig from './server/wITConfig';
import {
   isProduction, isWatching, host, browserHotPort, root
} from './universal/config';

const getEntry = () => {
   let entry = [`${root}/browser/src/main`];

   if(isWatching) {
      entry.push(`webpack-hot-middleware/client?path=http://${host}:` +
         `${browserHotPort}/__webpack_hmr&reload=true`);
   }

   return entry;
};

const getStyleLoader = () => {
   const minimize = isProduction? 'minimize' : '';
   const ending = `css?${minimize}!postcss!sass`;

   if(isWatching) {
      return 'style!' + ending;
   } else {
      return ExtractTextPlugin.extract('style', ending);
   }
};

const getPlugins = () => {
   let plugins = [
      new webpack.NoErrorsPlugin(),
      new webpack.optimize.DedupePlugin(),
      new webpack.optimize.OccurenceOrderPlugin(),
      new CopyWebpackPlugin([
         {from: `${root}/browser/src/images`, to: 'images'},
         {from: `${root}/browser/src/downloads`, to: 'downloads'}
      ]),
      new webpack.DefinePlugin({
         'process.env': Object.keys(process.env).reduce((o, k) => {
            o[k] = JSON.stringify(process.env[k]);
            return o;
         }, {})
      })
   ];

   if(isProduction) {
      plugins.push(new webpack.optimize.UglifyJsPlugin({
         compress: {warnings: false},
         sourceMap: false,
         mangle: false
      }));
   }

   const wITP = new WebpackIsomorphicToolsPlugin(wITConfig);

   if(isWatching) {
      plugins.push(new webpack.HotModuleReplacementPlugin());
      plugins.push(wITP.development());
   } else {
      plugins.push(new ExtractTextPlugin('[name].css'));
      plugins.push(wITP);
   }

   return plugins;
};

export default {
   entry: {main: getEntry()},
   browserHotPort,
   module: {
      loaders: [
         {
            test: /.(png|woff(2)?|eot|ttf|svg)(\?[a-z0-9=\.]+)?$/,
            loader: 'url?limit=1000000'
         },
         {
            test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot|xml|ico)$/,
            loader: 'file?name=[path][name].[ext]'
         },
         {
            test: /\.js$/,
            loaders: ['react-hot', 'babel?cacheDirectory'],
            exclude: /node_modules/
         },
         {test: /\.sass$/, loader: getStyleLoader()},
         {test: /\.html$/, loader: 'html'},
         {test: /\.json$/, loader: 'json'}
      ]
   },
   output: {
      path: `${root}/browser/dist`,
      filename: '[name].js',
      publicPath: isWatching? `http://${host}:${browserHotPort}/` : '/'
   },
   plugins: getPlugins(),
   postcss: [autoprefixer({browsers: ['last 2 versions']})]
};
