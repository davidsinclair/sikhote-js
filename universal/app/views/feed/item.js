import Helmet from 'react-helmet';
import React from 'react';
import Fallback from '../fallback';

export default props => {
   let d = props.route.items.find(i => i.slug === props.routeParams.item);
   let gallery = [];

   if(!d) {
      return (
         <Fallback />
      );
   }

   if(d.type === 'project') {
      for(let i = 1; i <= d.images; i++) {
         gallery.push(
            <li key={i}>
               <img
                  alt={d.title}
                  src={require(
                     `../../../../browser/src/images/projects/${d.slug}-` +
                     `${i}.jpg`
                  )}
               />
            </li>
         );
      }
   }

   return (
      <div className='view feed-item'>
         <Helmet title={`${props.route.title} · ${d.title}`} />
         <div className='content'>
            <h1>{d.title}</h1>
            <article>
               <div dangerouslySetInnerHTML={{__html: d.content}} />
               {gallery.length?
                  <ul className='gallery'>{gallery}</ul> : null
               }
            </article>
         </div>
      </div>
   );
};