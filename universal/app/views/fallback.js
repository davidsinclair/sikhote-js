import React from 'react';
import Helmet from 'react-helmet';
import content from '../../content.json';

export default () => (
   <div className='view'>
      <Helmet title={content.pages.fallback.title} />
      <div
         className='content'
         dangerouslySetInnerHTML={{__html: content.pages.fallback.content}}
      />
   </div>
);