import Helmet from 'react-helmet';
import React from 'react';
import {description} from '../../../package.json';
import content from '../../content.json';

export default () => (
   <div className='view home'>
      <Helmet title={description} />
      <div
         className='content'
         dangerouslySetInnerHTML={{__html: content.pages.home.content}}
      />
   </div>
);