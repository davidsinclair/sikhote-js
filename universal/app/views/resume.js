import Helmet from 'react-helmet';
import React from 'react';
import content from '../../content.json';

export default () => (
   <div className='view'>
      <Helmet title={content.pages.resume.title} />
      <div
         className='content'
         dangerouslySetInnerHTML={{__html: content.pages.resume.content}}
      />
   </div>
);