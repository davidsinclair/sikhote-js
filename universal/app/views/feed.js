import Helmet from 'react-helmet';
import React from 'react';
import {Link} from 'react-router';

export default props => (
   <div className='view feed'>
      <Helmet title={props.route.title} />
      <ul className='items'>
         {props.route.items.map((o, i) => (
            <li key={i} className={o.type}>
               <Link
                  to={`${props.location.pathname}/${o.slug}`}
                  style={{
                     backgroundImage: o.type === 'project'?
                        `url(${require(
                           '../../../browser/src/images/projects/' +
                           `${o.slug}-1.jpg`
                        )})` : ''
                  }}
               >
                  <h2>{o.title}</h2>
                  {o.type === 'thought'? <article>{o.summary}</article> : null}
               </Link>
            </li>
         ))}
      </ul>
   </div>
);