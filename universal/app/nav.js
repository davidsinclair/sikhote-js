import {Link, IndexLink} from 'react-router';
import React from 'react';

export default () => (
   <nav className='nav'>
      <ul>
         <li><IndexLink to='/' activeClassName='active'>home</IndexLink></li>
         <li><Link to='/projects' activeClassName='active'>projects</Link></li>
         <li><Link to='/thoughts' activeClassName='active'>thoughts</Link></li>
      </ul>
   </nav>
);
