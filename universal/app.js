import Helmet from 'react-helmet';
import React from 'react';
import {RouteTransition} from 'react-router-transition';
import Nav from './app/nav';
import {name} from './content.json';

export default props => (
   <div
      className={`app ${props.location.pathname === '/resume'? 'resume' : ''}`}
   >
      <Helmet titleTemplate={`${name} · %s`} />
      <Nav />
      <RouteTransition
         pathname={props.location.pathname}
         atEnter={{opacity: 0}}
         atLeave={{opacity: 0}}
         atActive={{opacity: 1}}
         className='views'
      >{props.children}</RouteTransition>
   </div>
);
