import React from 'react';
import {IndexRoute, Route} from 'react-router';
import App from './app';
import Fallback from './app/views/fallback';
import Home from './app/views/home';
import Resume from './app/views/resume';
import Feed from './app/views/feed';
import Item from './app/views/feed/item';
import content from './content.json';

const projects = content.feed.filter(i => i.type === 'project');
const thoughts = content.feed.filter(i => i.type === 'thought');

export default (
   <Route path='/' component={App}>
      <IndexRoute component={Home} />
      <Route path='resume' component={Resume} />
      <Route path='projects'>
         <IndexRoute
            component={Feed}
            className='feed'
            title='Projects'
            items={projects}
         />
         <Route
            path=':item'
            className='feed-item projects'
            component={Item}
            title='Projects'
            items={projects}
         />
      </Route>
      <Route path='thoughts'>
         <IndexRoute
            component={Feed}
            title='Thoughts'
            items={thoughts}
            className='feed'
         />
         <Route
            path=':item'
            className='feed-item thoughts'
            component={Item}
            title='Thoughts'
            items={thoughts}
         />
      </Route>
      <Route path='*' component={Fallback} />
   </Route>
);
